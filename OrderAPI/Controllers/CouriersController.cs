﻿using Blocks.Contracts.OrdersManagement;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OrderAPI.Core.Abstractions;
using OrderAPI.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrderAPI.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]

    public class CouriersController : ControllerBase
    {

        private readonly ILogger<CouriersController> _logger;
        private readonly IRepository<Courier> _courierRepository;

        public CouriersController(ILogger<CouriersController> logger, IRepository<Courier> courierRepository)
        {
            _logger = logger;
            _courierRepository = courierRepository;
        }

        /// <summary>
        /// Получить всех курьеров
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<CouriersResponse>> GetCouriersAsync()
        {
            var couriers = await _courierRepository.GetAllAsync();

            var couriersModelList = couriers.Select(x => new CouriersResponse(x)).ToList();

            return couriersModelList;
        }

        [HttpPost]
        public async Task<ActionResult> CreateCourierAsync()
        {
            throw new NotImplementedException();
        }

        [HttpPut]
        public async Task<ActionResult> UpdateCourierAsync()
        {
            throw new NotImplementedException();
        }

        [HttpDelete]
        public async Task<ActionResult> DeleteCourierAsync()
        {
            throw new NotImplementedException();
        }
    }
}
