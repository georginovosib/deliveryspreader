﻿using Blocks.Contracts.OrdersManagement;
using System;

namespace OrderAPI.WebHost.Models
{
    public class ProductsResponse
    {
        public ProductsResponse(Product product)
        {
            Id = product.Id;
            Name = product.Name;
            Description = product.Description;
            Price = product.Price;
            DeliveryPrice = product.DeliveryPrice;
        }

        public Guid Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public double Price { get; set; }

        public double DeliveryPrice { get; set; }
    }
}
