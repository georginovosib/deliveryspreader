﻿using Blocks.Contracts.OrdersManagement;
using System.Threading.Tasks;

namespace Delivery.ProductOrder.Core.Abstractions
{
    public interface IGivingOrderDeliveryGateway
    {
        Task GiveOrderToAll(Order order);
    }
}
