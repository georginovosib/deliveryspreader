﻿using Blocks.Contracts.BaseEntitys;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Delivery.ProductOrder.Core.Abstractions
{
    public interface IRepository<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();

        Task<IEnumerable<T>> GetAllAsync(string property);

        Task<T> GetByIdAsync(Guid id);

        Task<T> GetByIdAsync(Guid id, string property);

        Task CreateAsync(T t);

        Task DeleteAsync(T t);

        Task UpdateAsync(T t);
    }
}
