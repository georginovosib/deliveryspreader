using Blocks.Contracts.Abstractions;
using Blocks.Contracts.Models;
using Blocks.Contracts.OrdersManagement;
using Delivery.ProductOrder.Core.Abstractions;

using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Delivery.ProductOrder.DataAccess.Services
{
    public class CustomerService: ICachingService<Customer>
    {
        private readonly IMemoryCache _cache;
        private readonly IRepository<Customer> _repository;

        public CustomerService(IRepository<Customer> repository, IMemoryCache cache)
        {
            _cache = cache;
            _repository = repository;
        }

        public async Task<Customer> GetByIdAsync(Guid id)
        {
            Customer customer;
            if (!_cache.TryGetValue(id, out customer))
            {
                customer = await _repository.GetByIdAsync(id);
                _cache.Set(id, customer, new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5)
                });
            }
            return customer;
        }

        public async Task<Customer> CreateAsync(Customer request)
        {
            var customer = new Customer
            {
                Id = Guid.NewGuid(),
                FirstName = request.FirstName,
                Email = request.Email
            };

            await _repository.CreateAsync(customer);

            _cache.Set(customer.Id, customer, new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5)
            });
            return customer;

        }

        public async Task UpdateAsync(Customer customer)
        {
            _cache.Set(customer.Id, customer, new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5)
            });
            await _repository.UpdateAsync(customer);

        }

        public async Task DeleteAsync(Guid id)
        {
            var customer = await this.GetByIdAsync(id);
            await _repository.DeleteAsync(customer);
            _cache.Remove(id);


        }

        public async Task<Customer> GetByIdAsync(Guid id, string str)
        {
            throw new NotImplementedException();
        }

    }
}
