﻿using Blocks.Contracts.OrdersManagement;
using Delivery.ProductOrder.Core.Abstractions;

using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Blocks.Contracts.Models;
using Blocks.Contracts.Abstractions;

namespace Delivery.ProductOrder.DataAccess.Services
{
    public class OrderService: ICachingService<Order>
    {
        private readonly IMemoryCache _cache;
        private readonly IRepository<Order> _orderRepository;
        private readonly IRepository<Product> _productRepository;

        public OrderService(IMemoryCache cache, IRepository<Order> orderRepository)
        {
            _cache = cache;
            _orderRepository = orderRepository;
        }

        public async Task<Order> CreateAsync(Order order)
        {

            await _orderRepository.CreateAsync(order);

            _cache.Set(order.Id, order, new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5)
            });
            return order;
        }

        public async Task<Order> GetByIdAsync(Guid id)
        {
            Order order;
            if (!_cache.TryGetValue(id, out order))
            {
                order = await _orderRepository.GetByIdAsync(id);
                if (order != null)
                {
                    _cache.Set(id, order, new MemoryCacheEntryOptions
                    {
                        AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5)
                    });
                }

            }
            return order;
        }

        public async Task<Order> GetByIdAsync(Guid id, string property)
        {
            Order order;
            if (!_cache.TryGetValue(id, out order) || order.Comment != property)
            {
                order = await _orderRepository.GetByIdAsync(id, property);
                if (order != null)
                {
                    _cache.Set(id, order, new MemoryCacheEntryOptions
                    {
                        AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5)
                    });
                }

            }
            return order;
        }

        public async Task UpdateAsync(Order order)
        {

            _cache.Set(order.Id, order, new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5)
            });
            await _orderRepository.UpdateAsync(order);

        }

        public async Task DeleteAsync(Guid id)
        {

            var product = await this.GetByIdAsync(id);
            await _orderRepository.DeleteAsync(product);
            _cache.Remove(id);

        }



    }
}
