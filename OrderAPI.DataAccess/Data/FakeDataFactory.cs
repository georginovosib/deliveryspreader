﻿using Blocks.Contracts.OrdersManagement;
using System;
using System.Collections.Generic;

namespace OrderAPI.DataAccess.Data
{
    public static class FakeDataFactory
    {

        public static IEnumerable<Order> Orders => new List<Order>()
        {
            new Order()
            {
                Id = Guid.Parse("123456d6-d8d5-1111-9c7b-eb9f14e1a32f"),
                Status = OrderStatus.New,
                CreationDate = DateTime.Now,
                CompletionDate = null,
                Comment = "Комментарий хороший",
                CustomerId = Guid.Parse("123456d6-1111-4a11-9c7b-eb9f14e1a111"),
                CourierId = null
            },
            new Order()
            {
                Id = Guid.Parse("223456d6-d8d5-2222-9c7b-eb9f14e1a32f"),
                Status = OrderStatus.New,
                CreationDate = DateTime.Now,
                CompletionDate = null,
                Comment = "Комментарий плохой",
                CustomerId = Guid.Parse("123456d6-1111-4a11-9c7b-eb9f14e1a222"),
                CourierId = null
            },
            new Order()
            {
                Id = Guid.Parse("323456d6-d8d5-3333-9c7b-eb9f14e1a32f"),
                Status = OrderStatus.New,
                CreationDate = DateTime.Now,
                CompletionDate = null,
                Comment = "Комментарий",
                CustomerId = Guid.Parse("123456d6-1111-4a11-9c7b-eb9f14e1a333"),
                CourierId = null
            }
        };

        public static IEnumerable<Product> Products => new List<Product>()
        {
            new Product()
            {
                Id = Guid.Parse("123456d6-1111-4a11-9c7b-eb9f14e1a32f"),
                Name = "Притяжение",
                Description = "Букет",
                Price = 1800,
                DeliveryPrice = 500
            },
            new Product()
            {
                Id = Guid.Parse("123456d6-2222-4a11-9c7b-eb9f14e1a32f"),
                Name = "Мелодия цветов",
                Description = "Букет",
                Price = 2800,
                DeliveryPrice = 500
            },
            new Product()
            {
                Id = Guid.Parse("123456d6-3333-4a11-9c7b-eb9f14e1a32f"),
                Name = "Летняя свежесть",
                Description = "Букет",
                Price = 3800,
                DeliveryPrice = 500
            },
            new Product()
            {
                Id = Guid.Parse("123456d6-4444-4a11-9c7b-eb9f14e1a32f"),
                Name = "Цветочная фантазия",
                Description = "Букет",
                Price = 6300,
                DeliveryPrice = 500
            },
            new Product()
            {
                Id = Guid.Parse("123456d6-5555-4a11-9c7b-eb9f14e1a32f"),
                Name = "Искорка радости",
                Description = "Букет",
                Price = 38000,
                DeliveryPrice = 500
            }
        };

        public static IEnumerable<OrderProducts> OrderProducts => new List<OrderProducts>()
        {
            new OrderProducts
            {
                OrdersId = Guid.Parse("123456d6-d8d5-1111-9c7b-eb9f14e1a32f"),
                ProductsId = Guid.Parse("123456d6-1111-4a11-9c7b-eb9f14e1a32f")
            },
            new OrderProducts
            {
                OrdersId = Guid.Parse("123456d6-d8d5-1111-9c7b-eb9f14e1a32f"),
                ProductsId = Guid.Parse("123456d6-2222-4a11-9c7b-eb9f14e1a32f")
            },
            new OrderProducts
            {
                OrdersId = Guid.Parse("123456d6-d8d5-1111-9c7b-eb9f14e1a32f"),
                ProductsId = Guid.Parse("123456d6-3333-4a11-9c7b-eb9f14e1a32f")
            },
            new OrderProducts
            {
                OrdersId = Guid.Parse("223456d6-d8d5-2222-9c7b-eb9f14e1a32f"),
                ProductsId = Guid.Parse("123456d6-4444-4a11-9c7b-eb9f14e1a32f")
            },
            new OrderProducts
            {
                OrdersId = Guid.Parse("223456d6-d8d5-2222-9c7b-eb9f14e1a32f"),
                ProductsId = Guid.Parse("123456d6-5555-4a11-9c7b-eb9f14e1a32f")
            },
            new OrderProducts
            {
                OrdersId = Guid.Parse("323456d6-d8d5-3333-9c7b-eb9f14e1a32f"),
                ProductsId = Guid.Parse("123456d6-5555-4a11-9c7b-eb9f14e1a32f")
            }
        };
        public static IEnumerable<Courier> Couriers => new List<Courier>()
        {
            new Courier()
            {
                Id = Guid.Parse("323456d6-d8d5-3333-1111-eb9f14e1a32f"),
                FirstName = "Михаил",
                LastName =  "Быстров",
                Email = "ob@ob.ru"
            },
            new Courier()
            {
                Id = Guid.Parse("323456d6-d8d5-3333-2222-eb9f14e1a32f"),
                FirstName = "Игорь",
                LastName =  "Максимов",
                Email = "sm@sm.ru"
            }
        };

        public static IEnumerable<Customer> Customers => new List<Customer>()
        {
            new Customer()
            {
                Id = Guid.Parse("123456d6-1111-4a11-9c7b-eb9f14e1a111"),
                FirstName = "Василий",
                Address = "Тверская, 19",
                Email = "vv@vv.ru"
            },
            new Customer()
            {
                Id = Guid.Parse("123456d6-1111-4a11-9c7b-eb9f14e1a222"),
                FirstName = "Генадий",
                Address = "Светлая, 82",
                Email = "gg@gg.ru"
            },
            new Customer()
            {
                Id = Guid.Parse("123456d6-1111-4a11-9c7b-eb9f14e1a333"),
                FirstName = "Кирилл",
                Address = "Добролюбова, 14",
                Email = "kk@kk.ru"
            }
        };
    }

    

    public class OrderProducts
    {
        public Guid OrdersId { get; set; }

        public Guid ProductsId { get; set; }
    }
}
