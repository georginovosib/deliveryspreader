﻿using Blocks.Contracts.BaseEntitys;
using System;
using System.Collections.Generic;

namespace Blocks.Contracts.OrdersManagement
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int Price { get; set; }

        public int Amount { get; set; }

        public int DeliveryPrice { get; set; }

        public List<Guid> OrderIds { get; set; }
    }
}
