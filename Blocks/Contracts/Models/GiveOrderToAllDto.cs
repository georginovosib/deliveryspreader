﻿using System;
using System.Collections.Generic;

namespace Blocks.Contracts.Models
{
    public class GiveOrderToAllDto
    {
        public Guid OrderId { get; set; }

        public Guid CustomerId { get; set; }

        public List<Guid> ProductsId { get; set; }

        public bool IsPaid { get; set; }

        public string Comment { get; set; }

        public DateTime CreationDate { get; set; }

    }
}
