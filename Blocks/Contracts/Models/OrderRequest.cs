﻿using System;

namespace Blocks.Contracts.Models
{
    public class OrderRequest
    {
        public Guid ProductId { get; set; }
        public int Amount { get;set; }
    }
}
