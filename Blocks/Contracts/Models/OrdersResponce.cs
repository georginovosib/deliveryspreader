﻿using Blocks.Contracts.OrdersManagement;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Blocks.Contracts.Models
{
    public class OrdersResponce
    {
        public OrdersResponce(Order order)
        {
            Id = order.Id;
            Status = order.Status;
            IsPaid = order.IsPaid;
            CreationDate = order.CreationDate;
            CourierAssignedDate = order.CourierAssignedDate;
            Comment = order.Comment;
            Products = order.Products.Select(x => new ProductsShortResponse(x)).ToList();
            if (order.Customer != null) CustomerFirstName = order.Customer.FirstName;
            if (order.Customer != null) CustomerEmail = order.Customer.Email;
            if (order.Customer != null) CustomerAddress = order.Customer.Address;
            if (order.CourierId != null) CourierId = order.CourierId.Value;
            if (order.Courier != null)
            {
                CourierFirstName = order.Courier.FirstName;
                CourierLastName = order.Courier.LastName;
                CourierEmail = order.Courier.Email;
            }
        }

        public Guid Id { get; set; }
        public OrderStatus Status { get; set; }
        public bool IsPaid { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? CourierAssignedDate { get; set; }
        public DateTime? CompletionDate { get; set; }
        public string Comment { get; set; }
        public List<ProductsShortResponse> Products { get; set; }
        public string CustomerFirstName { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerAddress { get; set; }
        public Guid CourierId { get; set; }
        public string CourierFirstName { get; set; }
        public string CourierLastName { get; set; }
        public string CourierEmail { get; set; }

    }

    public class ProductsShortResponse
    {
        public ProductsShortResponse(Product product)
        {
            Name = product.Name;
            Price = product.Price;
            Amount = product.Amount;
        }

        public string Name { get; set; }

        public double Price { get; set; }

        public int Amount { get; set; }
    }
}

