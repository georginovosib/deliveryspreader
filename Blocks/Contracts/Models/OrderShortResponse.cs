﻿using Blocks.Contracts.OrdersManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blocks.Contracts.Models
{
    public class OrderShortResponse
    {
        public OrderShortResponse()
        { }
        public OrderShortResponse(Order order)
        {
            if (order != null)
            {
                Id = order.Id;
                Status = order.Status;
                Comment = order.Comment;
            }
        }

        public Guid Id { get; set; }

        public OrderStatus Status { get; set; }

        public string Comment { get; set; }


    }
}
