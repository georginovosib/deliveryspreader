﻿using Blocks.Contracts.OrdersManagement;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Blocks.Contracts.Models
{
    public class CouriersResponse
    {
        public CouriersResponse(Courier courier)
        {
            Id = courier.Id;
            FirstName = courier.FirstName;
            LastName = courier.LastName;
            Email = courier.Email;
            if (courier.OrderIds != null)
            {
                OrderIds = courier.OrderIds;
            }
        }

        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public List<Guid> OrderIds { get; set; }
    }
}
