﻿using System;

namespace Blocks.Contracts.Models
{
    public class GiveProductRequest
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public int Amount { get; set; }

        public string Description { get; set; }

        public int Price { get; set; }

        public int DeliveryPrice { get; set; }
    }
}
