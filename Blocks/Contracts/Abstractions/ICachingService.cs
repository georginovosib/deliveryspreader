﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Blocks.Contracts.BaseEntitys;

namespace Blocks.Contracts.Abstractions
{
    public interface ICachingService<T> where T : BaseEntity
    {
        Task<T> CreateAsync (T entity);

        Task<T> GetByIdAsync (Guid id);

        Task<T> GetByIdAsync(Guid id, string property);

        Task UpdateAsync (T entity);

        Task DeleteAsync(Guid id);
    }
}
