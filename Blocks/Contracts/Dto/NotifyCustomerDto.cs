﻿using Blocks.Contracts.OrdersManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blocks.Contracts.Dto
{
    public class NotifyCustomerDto
    {
        public NotifyCustomerDto(Order order, Guid customerId)
        {
            Id = order.Id;
            Status = order.Status;
            IsPaid = order.IsPaid;
            CreationDate = order.CreationDate;
            CourierAssignedDate = order.CourierAssignedDate;
            CompletionDate = order.CompletionDate;
            Comment = order.Comment;
            CustomerId = customerId;
            Customer = order.Customer;
            CourierId = order.CourierId;
            Courier = order.Courier;
            Products = order.Products;
            Event = "Customer_Set";
        }

        public string Event { get; set; }

        public Guid Id { get; set; }

        public OrderStatus Status { get; set; }

        public bool IsPaid { get; set; }

        public DateTime CreationDate { get; set; }

        public DateTime? CourierAssignedDate { get; set; }

        public DateTime? CompletionDate { get; set; }

        public string Comment { get; set; }

        public Guid? CustomerId { get; set; }

        public Customer Customer { get; set; }

        public Guid? CourierId { get; set; }

        public Courier Courier { get; set; }

        public virtual ICollection<Product> Products { get; set; }
    }
}
