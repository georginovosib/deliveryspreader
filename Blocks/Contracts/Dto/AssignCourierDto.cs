﻿using Blocks.Contracts.OrdersManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Blocks.Contracts.Dto
{
    public class AssignCourierDto
    {
        [JsonConstructor]
        public AssignCourierDto() {
        }

        public AssignCourierDto(Courier courier, Guid orderId)
        {
            CourierId = courier.Id;
            OrderId = orderId;
            FirstName = courier.FirstName;
            LastName = courier.LastName;
            Email = courier.Email;
            Event = "Courier_Assigned";
        }

       
        public Guid CourierId { get; set; }

        public Guid OrderId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public string Event { get; set; }

    }
}
