﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Blocks.Contracts.Dto
{
    public class AddProductRangeDto
    {
        [JsonConstructor]
        public AddProductRangeDto() { }

        public AddProductRangeDto(IEnumerable<AddProductDto> dtos, Guid orderId)
        {
            DtoCollection = dtos;
            OrderId = orderId;
            Event = "ProductRange_Sent";
        }

        public IEnumerable<AddProductDto> DtoCollection { get; set; }
        public Guid OrderId { get; set; }
        public string Event { get; set; }
    }
}
