﻿using Blocks.Contracts.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Blocks.Helpers
{
    public enum EventType
    {
        ProductRange_Sent,
        Product_Sent,
        Courier_Assigned,
        Undetermined
    }
    public static class EventHelper
    {
        public static EventType DetermineEvent(string notificationMessage)
        {
            Console.WriteLine("--> Determining Event");
            var eventType = JsonSerializer.Deserialize<GenericEventDto>(notificationMessage);

            switch (eventType.Event)
            {
                case "ProductRange_Sent":
                    Console.WriteLine("--> ProductRange_Sent Event detected");
                    return EventType.ProductRange_Sent;
                case "Product_Sent":
                    Console.WriteLine("--> Product_Sent Event detected");
                    return EventType.Product_Sent;
                case "Courier_Assigned":
                    Console.WriteLine("--> Courier_Assigned Event detected");
                    return EventType.Courier_Assigned;
                default:
                    Console.WriteLine("--> Event type not determined");
                    return EventType.Undetermined;
            }
        }
    }
}
