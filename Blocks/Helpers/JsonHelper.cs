﻿using System.Text.Json;

namespace Blocks.Helpers
{
    public static class JsonHelper
    {
        public static string ToSerialize(object obj)
        {
            return JsonSerializer.Serialize(obj);
        }
        public static T ToDeSerialize<T>(string json)
        {
            return JsonSerializer.Deserialize<T>(json);
        }
    }
}
