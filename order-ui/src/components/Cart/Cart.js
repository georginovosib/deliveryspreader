﻿import React, {useEffect, useState} from "react";
import classes from './Cart.module.css';
import Cookies from "universal-cookie/es6";
import {useNavigate} from "react-router-dom";
import Button from "../UI/Button/Button";
import Loader from "../UI/Loader/Loader";
import axios from "../../axios/axios-productStore";
import axiosOrder from "../../axios/axios-productOrder"
import {CartItem} from "./CartItem/CartItem";


const Cart = () => {
    const navigate = useNavigate();
    const [customerId] = useState('123456d6-1111-4a11-9c7b-eb9f14e1a111'); // пока делаем айдишник хардкодом, дальше брать из профиля
    const [cookie] = useState(new Cookies());
    const [totalSum, setTotalSum] = useState(0);
    const [products, setProducts] = useState([]);
    const [loading, setLoading] = useState(true);
    const [orderId, setOrder] = useState();
    const [flag, setFlag] = useState(false);

    useEffect(() => {
        const cookieProducts = cookie.get('products');
        if (cookieProducts && cookieProducts.length > 0) {
            async function createOrder(){
                try {
                    const response = await axiosOrder.post('Orders',
                    null,
                    {
                        params: {
                            customerId: customerId //todo - pass here param from profile
                        }
                    });
                    setOrder(response.data.id);
                    
                } catch (e) {
                    window.alert('ошибка при создании заказа: ', e);
                    setOrder('123456d6-1111-4a11-9c7b-eb9f14e1a32f') // todo remove
                }
            }

            createOrder();
            
            const totalS = cookieProducts.map(x => {
                return x.amount * x.product.price;
            }).reduce((sum, curr) => sum + curr);

            setTotalSum(totalS);
            setProducts([...cookieProducts]);

        }
        setLoading(false);
    }, [])

    function renderCart() {
        if (products.length > 0) {            
            
            const content = products.map((productDto, index) => {
                return (
                    <CartItem
                        className={index === products.length - 1 ? 'withoutBorder' : 'bottomBorder'}
                        key={index}
                        product={productDto.product}
                        amount={productDto.amount}
                    />
                )
            })
            return (
                <React.Fragment>
                    {content}
                    <br/>
                    <span style={{fontSize: '2rem', textAlign: 'right'}}>Total sum: <p style={{color: "#db6019"}}>{totalSum}</p></span>
                    <Button
                        type='primary'
                        onClick={createOrder}
                    >Create order</Button>
                </React.Fragment>
            )
        } else {            
            return (
                <h3>Cart is empty</h3>
            )
        }
    }

    async function createOrder() {             
        try {            
            const request = products.map(x => {
                return {
                    productId: x.product.id,
                    amount: x.amount,
                    orderId: orderId
                }
            });
            for (let item of request) {                
                await axios.put(`Products?productId=${item.productId}&orderId=${orderId}&amount=${item.amount}`);
            }

            // const response = await axios.post(
            //     'Orders',
            //     request,
            //     );
            cookie.remove('products');
            navigate('/');
        } catch (e) {
            window.alert('ошибка при сохранении заказа: ', e);
        }

    }

    let content

    if (loading) {
        content = <Loader/>
    } else {
        content = renderCart()
    }

    return (
        <div className={classes.Cart}>
            {content}
        </div>
    )
}

export default Cart;