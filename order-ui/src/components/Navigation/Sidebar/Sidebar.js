﻿import React, {Component} from "react";
import classes from './Sidebar.module.css'
import {NavLink} from "react-router-dom";
import Backdrop from "../../UI/Backdrop/Backdrop";

const links = [
    {to: '/', label: 'Product list'},
    {to: '/cart', label: 'Cart'},
    {to: '/about', label: 'About'}
]

export default class Sidebar extends Component {

    clickHandler = () => {
        this.props.onClose();
    }
    
    renderLinks () {
        return links.map((link, index) => {
            return (
                <li key={index}>
                    <NavLink
                        to={link.to}
                        className={(navData) => navData.isActive ? classes.active : null}
                        onClick={this.clickHandler}
                    >
                        {link.label}
                    </NavLink>                    
                </li>
            )
        })
    }
    
    render() {
        const cls = [classes.Sidebar];

        if (!this.props.isOpen) {
            cls.push(classes.close)
        }
        
        return (
            <React.Fragment>
                <div className={cls.join(' ')}>
                    <ul>
                        {this.renderLinks()}
                    </ul>
                </div>
                {this.props.isOpen ? <Backdrop onClick={this.props.onClose}/> : null}
            </React.Fragment>
        )
    }
}