﻿import React, {Component} from "react";
import classes from './ProductList.module.css'
import axios from "../../axios/axios-productStore";
import Loader from "../UI/Loader/Loader";
import {NavLink} from "react-router-dom";
import Product from "../Product/Product";

export default class ProductList extends Component {
    state = {
        products: [],
        loading: true
    }

    renderProducts() {
        return this.state.products.map(product => {
            return (
                <NavLink
                    key={product.id}
                    to={`/product/${product.id}`}
                    state={{product: product}}
                >
                    <Product
                        key={product.id}
                        product={product}
                    />
                </NavLink>
            )
        })
    }

    async componentDidMount() {
        const products = [];
        try {
            const response = await axios('Products');
            products.push(...response.data.$values);
        } catch (e) {
            window.alert(e);
            console.log(e);
        }

        this.setState({
            loading: false,
            products
        });
    }

    //todo: add pagination
    render() {

        return (            
            <div className={classes.ProductList}>
                {(this.state.loading && <Loader/>) || <div>{this.renderProducts()}</div>}
            </div>
        )
    }

}