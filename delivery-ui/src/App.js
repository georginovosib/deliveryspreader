import React, {useState} from 'react';
import './App.css';
import {OrderList} from "./components/OrderList/OrderList";
import {Navigate, Route, Routes} from "react-router-dom";
import {Layout} from "./hoc/Layout/Layout";
import {Profile} from "./components/Profile/Profile";

function App() {
    const [authorized, setAuthorized] = useState(true);

    return (
        <Layout>
            <Routes>
                }
                {/*<Route path='/auth' exact element={<Auth onAuthorizationChange={this.onAuthorizationChange}/>}/>*/}
                <Route path='/' exact element={authorized ? <OrderList/> : <Navigate to='/auth'/>}/>
                <Route path='/profile' exact element={authorized ? <Profile/> : <Navigate to='/auth'/>}/>
            </Routes>
        </Layout>
    );
}

export default App;
