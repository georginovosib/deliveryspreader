﻿import React, {useEffect} from "react";
import styles from './Profile.module.css'
import {Table} from "react-bootstrap";
import {useDispatch, useSelector} from "react-redux";
import {loadProfileAsync} from "../../store/reducers/profileSlice";
import Loader from "../UI/Loader/Loader";
import {ActivityInfo} from "../ActivityInfo/ActivityInfo";

export const Profile = () => {
    const dispatch = useDispatch();
    const courier = useSelector(state => state.profile.courier);
    const status = useSelector(state => state.profile.status);
    const error = useSelector(state => state.profile.error);


    useEffect(() => {
        if (status === 'idle') {
            dispatch(loadProfileAsync())
        }
    }, [status, dispatch])

    let content

    if (status === 'loading') {
        content = <Loader/>
    } else if (status === 'success') {
        content = <React.Fragment>
            <h1>{ courier.firstName } {courier.lastName}</h1>
            <Table bordered hover>
                <tbody>
                <tr>
                    <th>Name</th>
                    <th>{courier.firstName}</th>
                </tr>
                <tr>
                    <th>Surname</th>
                    <th>{courier.lastName}</th>
                </tr>
                <tr>
                    <th>Surname</th>
                    <th>{courier.email}</th>
                </tr>
                </tbody>
            </Table>
            <ActivityInfo/>
        </React.Fragment>
            
    } else if (status === 'failed') {
        content = <h1>{error}</h1>
    }

    return (
        <div className={styles.Profile}>
            {content}
        </div>
    );
}