﻿import React, {Component} from "react";
import styles from './Sidebar.module.css'
import {NavLink} from "react-router-dom";
import {Activity} from "../../Activity/Activity";

const links = [
    {to: '/', label: 'Order list'},
    {to: '/profile', label: 'Profile'},
    {to: '/logout', label: 'LogOut'}
]

export const Sidebar = () => {

    function renderLinks() {
        return links.map((link, index) => {
            return (
                    <li key={index}>
                        <NavLink
                            to={link.to}
                            className={(navData) => navData.isActive ? styles.active : null}
                        >
                            {link.label}
                        </NavLink>
                    </li>         
            )
        })
    }


    return (
        <div className={styles.Sidebar}>
            <ul>
                {renderLinks()}                
            </ul>
            <NavLink to={'/profile'}>
                <Activity/>
            </NavLink>
        </div>
    )    
}