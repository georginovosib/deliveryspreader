﻿import {createSlice, createAsyncThunk} from "@reduxjs/toolkit";
import axios from "../../axios/axios-delivery";
import {fetchOrdersAsync} from "./orderListSlice";

export const loadProfileAsync = createAsyncThunk(
    'profile/loadProfile',
    async() => {
        const response = await axios('Couriers');
        return response.data[0];
    }
)

export const profileSlice = createSlice({
    name: 'profile',
    initialState: {
        status: 'idle',
        error: null,
        courier: {
            id: '',
            firstName: '',
            lastName: '',
            email: ''
        },
        ordersInProgress: []
    },
    reducers: {
        
    },
    extraReducers(builder) {
        builder
            .addCase(loadProfileAsync.pending, state => {
                state.status = 'loading';
            })
            .addCase(loadProfileAsync.fulfilled, (state, action) => {
                state.status = 'success';
                state.courier = action.payload;
            })
            .addCase(fetchOrdersAsync.rejected, (state, action) => {
                state.status = 'failed';
                state.error = action.error.message;
            })
    }
})

export default profileSlice.reducer;