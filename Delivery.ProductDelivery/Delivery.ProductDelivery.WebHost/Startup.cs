using Delivery.ProductDelivery.Core.Abstractions;
using Delivery.ProductDelivery.DataAccess;
using Delivery.ProductDelivery.DataAccess.Repositories;
using Delivery.ProductDelivery.DataAccesss.DataInitializer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;
using Microsoft.Extensions.Hosting;
using Delivery.ProductDelivery.DataAccess.Services;
using Blocks.Contracts.OrdersManagement;
using Blocks.Contracts.Abstractions;
using Delivery.ProductDelivery.Integration.AsyncDataServices;

namespace Delivery.ProductDelivery.WebHost
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));
            //services.AddScoped<IDbInitializer, EfDbInitializer>();

            services.AddDbContext<DataContext>(x => {

                x.UseNpgsql(Configuration.GetConnectionString("ProductDeliveryConnection"));

            }, ServiceLifetime.Singleton);

            services.AddScoped<ICachingService<Courier>, CourierService>();
            services.AddMemoryCache();

            services.AddScoped<IMessageBusClient, MessageBusClient>();

            services.AddControllers();

            services.AddOpenApiDocument(options =>
            {
                options.Title = "Delivery.ProductDelivery API";
                options.Version = "1.0";
            });

            services.AddCors(option =>
            {
                option.AddDefaultPolicy(
                    policy =>
                    {
                        policy.WithOrigins("http://localhost:3000",
                                "https://localhost:3000", "http://localhost:3001",
                                "https://localhost:3001", "http://localhost:3002",
                                "https://localhost:3002")
                            .AllowAnyMethod()
                            .AllowAnyHeader();
                    });
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        //public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseOpenApi();
            app.UseCors();

            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            //dbInitializer.InitializeDb();
        }
    }
}
