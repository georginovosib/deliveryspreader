﻿using Blocks.Contracts.Dto;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using System;
using System.Text;
using System.Text.Json;

namespace Delivery.ProductDelivery.Integration.AsyncDataServices
{
    public class MessageBusClient : IMessageBusClient
    {

        private readonly IConfiguration _configuration;

        public MessageBusClient(IConfiguration configuration)
        {
            _configuration = configuration;

        }

        public void AssignCourierToOrder(AssignCourierDto dto)
        {
            var factory = new ConnectionFactory()
            {
                UserName = "admin",
                Password = "admin",
                HostName = _configuration["RabbitMQHost"],
                Port = int.Parse(_configuration["RabbitMQPort"]),
            };

            Console.WriteLine($"--> Trying to connect on: {factory.HostName}:{factory.Port}");

            try
            {
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare(exchange: "courierNotification", type: ExchangeType.Fanout);

                    Console.WriteLine("--> Connected to MessageBus");
                    channel.QueueDeclare(queue: "courierAssigned",
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);
                    channel.QueueBind(queue: "courierAssigned",
                                      exchange: "courierNotification",
                                      routingKey: "");


                    var message = JsonSerializer.Serialize(dto);

                    var body = Encoding.UTF8.GetBytes(message);

                    channel.BasicPublish(exchange: "courierNotification",
                        routingKey: "",
                        basicProperties: null,
                        body: body);
                    Console.WriteLine($"--> We have sent {message}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"--> Could not connect to the Message Bus: {ex.Message}");
                Console.WriteLine($"--> InnerException: {ex.InnerException.Message}");
                throw;
            }

        }

        public void NotifyCustomerAboutOrder(NotifyCustomerDto dto)
        {
            var factory = new ConnectionFactory()
            {
                UserName = "guest",
                Password = "guest",
                HostName = _configuration["RabbitMQHost"],
                Port = int.Parse(_configuration["RabbitMQPort"]),
            };

            Console.WriteLine($"--> Trying to connect on: {factory.HostName}:{factory.Port}");

            try
            {
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare(exchange: "customerNotification", type: ExchangeType.Fanout);

                    Console.WriteLine("--> Connected to MessageBus");
                    channel.QueueDeclare(queue: "orderAdded",
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);
                    channel.QueueBind(queue: "orderAdded",
                                      exchange: "customerNotification",
                                      routingKey: "");


                    var message = JsonSerializer.Serialize(dto);

                    var body = Encoding.UTF8.GetBytes(message);

                    channel.BasicPublish(exchange: "customerNotification",
                        routingKey: "",
                        basicProperties: null,
                        body: body);
                    Console.WriteLine($"--> We have sent {message}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"--> Could not connect to the Message Bus: {ex.Message}");
                Console.WriteLine($"--> InnerException: {ex.InnerException.Message}");
                throw;
            }
        }

       
    }
}
