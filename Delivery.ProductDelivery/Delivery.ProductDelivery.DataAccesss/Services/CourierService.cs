﻿using Blocks.Contracts.Abstractions;
using Blocks.Contracts.Models;
using Blocks.Contracts.OrdersManagement;
using Delivery.ProductDelivery.Core.Abstractions;

using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Delivery.ProductDelivery.DataAccess.Services
{
    public class CourierService: ICachingService<Courier>
    {
        private readonly IMemoryCache _cache;
        private readonly IRepository<Courier> _repository;

        public CourierService(IRepository<Courier> repository, IMemoryCache cache)
        {
            _cache = cache;
            _repository = repository;
        }

        public async Task<Courier> GetByIdAsync(Guid id)
        {
            Courier courier;
            if (!_cache.TryGetValue(id, out courier))
            {
                courier = await _repository.GetByIdAsync(id);
                _cache.Set(id, courier, new MemoryCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5)
                });
            }
            return courier;
        }

        public async Task<Courier> CreateAsync(Courier courier)
        {
            
            await _repository.CreateAsync(courier);

            _cache.Set(courier.Id, courier, new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5)
            });
            return courier;

        }

        public async Task UpdateAsync(Courier courier)
        {
            _cache.Set(courier.Id, courier, new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(5)
            });
            await _repository.UpdateAsync(courier);

        }

        public async Task DeleteAsync(Guid id)
        {
            var courier = await this.GetByIdAsync(id);
            await _repository.DeleteAsync(courier);
            _cache.Remove(id);


        }

        public async Task<Courier> GetByIdAsync(Guid id, string str)
        {
            throw new NotImplementedException();
        }
    }
}
