﻿using Blocks.Contracts.OrdersManagement;
using System;
using System.Collections.Generic;

namespace Delivery.ProductDelivery.DataAccess.Data
{
    public static class FakeDataFactory
    {
        
        public static IEnumerable<Courier> Couriers => new List<Courier>()
        {
            new Courier()
            {
                Id = Guid.Parse("323456d6-d8d5-3333-1111-eb9f14e1a32f"),
                FirstName = "Михаил",
                LastName =  "Быстров",
                Email = "ob@ob.ru",
            },
            new Courier()
            {
                Id = Guid.Parse("323456d6-d8d5-3333-2222-eb9f14e1a32f"),
                FirstName = "Игорь",
                LastName =  "Максимов",
                Email = "sm@sm.ru",
            }
        };

    }
}
