﻿using Blocks.Contracts;
using EventBus.Base.Standard;
using System;
using System.Threading.Tasks;

namespace DatabaseService.EventHandler
{

    public class CreatedOrderHandler : IIntegrationEventHandler<CreatedOrderEvent>
    {
        public CreatedOrderHandler()
        {
        }

        public async Task Handle(CreatedOrderEvent @event)
        {
            var request = @event.Orders;
            Console.WriteLine($"{request}");
        }
    }
}
