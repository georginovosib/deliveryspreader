﻿using Blocks.Contracts.Models;
using Blocks.Contracts.OrdersManagement;
using Delivery.ProductStore.Core.Abstractions;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Delivery.ProductStore.WebHost.Hubs
{
    public class GreetHub : Hub
    {
        private readonly IRepository<Product> _productRepository;

        public GreetHub(IRepository<Product> productRepository)
        {
            _productRepository = productRepository;
        }

        public async Task<IEnumerable<ProductsResponse>> GetProductAsync()
        {
            var products = await _productRepository.GetAllAsync();

            var productsModelList = products.Select(x => new ProductsResponse(x)).ToList();

            return productsModelList;
        }

        public async Task<ProductsResponse> GetProductByIdAsync(string id)
        {
            var product = await _productRepository.GetByIdAsync(Guid.Parse(id));

            var productid = product.Id;

            var name = product.Name;

            var price = product.Price;

            var deliveryprice = product.DeliveryPrice;

            await Clients.All.SendAsync("Receive", productid, name, price, deliveryprice);

            return new ProductsResponse(product);
        }
    }
}
