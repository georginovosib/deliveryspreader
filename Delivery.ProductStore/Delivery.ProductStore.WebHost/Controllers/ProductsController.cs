﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Delivery.ProductStore.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Delivery.ProductStore.WebHost.Services;
using Blocks.Contracts.OrdersManagement;
using Blocks.Contracts.Models;
using Blocks.Contracts.Abstractions;
using Delivery.ProductStore.Integration.AsyncDataServices;
using Blocks.Contracts.Dto;

namespace Delivery.ProductStore.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ProductsController : ControllerBase
    {
        
        private readonly ILogger<ProductsController> _logger;
        private readonly IRepository<Product> _productRepository;
        private readonly ICachingService<Product> _service;
        private readonly IGivingProductDeliveryGateway _givingProductDeliveryGateway;
        private readonly IGivingProductOrderGateway _givingProductOrderGateway;
        private readonly IMessageBusClient _messageBus;

        public ProductsController(ILogger<ProductsController> logger, IRepository<Product> productRepository,
            IGivingProductDeliveryGateway givingProductDeliveryGateway, IGivingProductOrderGateway givingProductOrderGateway,
            ICachingService<Product> service,
            IMessageBusClient messageBus)
        {
            
            _logger = logger;
            _productRepository = productRepository;
            _service = service;
            _givingProductDeliveryGateway = givingProductDeliveryGateway;
            _givingProductOrderGateway = givingProductOrderGateway;
            _messageBus = messageBus;
        }

        /// <summary>
        /// Получить все продукты
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<ProductsResponse>> GetProductAsync()
        {
            var products = await _productRepository.GetAllAsync();

            var productsModelList = products.Select(x => new ProductsResponse(x)).ToList();

            return productsModelList;
        }

        /// <summary>
        /// Получить продукт по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ProductsResponse> GetProductByIdAsync(Guid id)
        {
            var product = await _service.GetByIdAsync(id);

            return new ProductsResponse(product);
        }

        /// <summary>
        /// Создать продукт
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> CreateProductAsync(GiveProductRequest request)
        {
            Product product = new Product()
            {
                Id = Guid.NewGuid(),
                Name = request.Name,
                Amount = request.Amount,
                Description = request.Description,
                Price = request.Price,
                DeliveryPrice = request.DeliveryPrice
            };

            await _service.CreateAsync(product);

            await _givingProductDeliveryGateway.GiveProductToAll(product);

            await _givingProductOrderGateway.GiveProductToAll(product);

            return Ok("Добавлен продукт");
        }

        /// <summary>
        /// Обновить продукт
        /// </summary>
        /// <param name="id"></param>
        /// <param name="name"></param>
        /// <param name="amount"></param>
        /// <param name="description"></param>
        /// <param name="price"></param>
        /// <param name="deliveryprice"></param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult> UpdateProductAsync(Guid id,  string name, int amount, string description, int price, int deliveryprice)
        {
            var product = await _service.GetByIdAsync(id);

            if (product == null) 
                return BadRequest();

            if(name != null) 
                product.Name = name;

            // if(amount != 0)
            //     product.Amount = amount;

            if(description != null) 
                product.Description = description;

            if(price != 0) 
                product.Price = price;

            if(deliveryprice != 0) 
                product.DeliveryPrice = deliveryprice;

            await _service.UpdateAsync(product);

            return Ok("Данные обновлены");
        }

        /// <summary>
        /// Добавить продукт в заказ
        /// </summary>
        /// <param name="productId"><example>123456d6-1111-4a11-9c7b-eb9f14e1a32f</example></param>
        /// <param name="orderId"><example>123456d6-d8d5-1111-9c7b-eb9f14e1a32f</example></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult> AddProductToOrderAsync(Guid productId, Guid orderId, int amount)
        { 
            var product = await _service.GetByIdAsync(productId);
            if (product == null) 
                return NotFound();
           
            if (product.OrderIds is null)
            {
                product.OrderIds = new List<Guid>();
            }
            if (!product.OrderIds.Contains(orderId))
            {
                product.OrderIds.Add(orderId);
            }

            var dto = new AddProductDto(product, orderId, amount);

            Console.WriteLine($"--> Created product dto: {dto}");

            _messageBus.SendProductToOrder(dto);

            await _service.UpdateAsync(product);
            return Ok("Продукт добавлен в заказ");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="productIdAmountMap">Id, количество продукта: <example>123456d6-1111-4a11-9c7b-eb9f14e1a32f , 10</example></param>
        /// <param name="orderId"><example>123456d6-d8d5-1111-9c7b-eb9f14e1a32f</example></param>
        /// <returns></returns>
        [HttpPut("addrangeto={orderId:guid}")]
        public async Task<ActionResult> AddProductRangeToOrderAsync(Dictionary<Guid, int> productIdAmountMap, Guid orderId )
        {
            List<AddProductDto> productDtos = new List<AddProductDto>();
            foreach (var id in productIdAmountMap.Keys)
            {
                var product = await _service.GetByIdAsync(id);
                var amount = productIdAmountMap[id];

                if (product.OrderIds is null)
                {
                    product.OrderIds = new List<Guid>();
                }
                if (!product.OrderIds.Contains(orderId))
                {
                    product.OrderIds.Add(orderId);
                }

                var dto = new AddProductDto(product, orderId, amount);
                Console.WriteLine($"--> Created product dto: {dto}");
                await _service.UpdateAsync(product);
                productDtos.Add(dto);

            }

            AddProductRangeDto dtos = new AddProductRangeDto(productDtos, orderId);

            _messageBus.SendProductRangeToOrder(dtos);

            return Ok("Продукты добавлен в заказ");

        }



        /// <summary>
        /// Удалить продукт
        /// </summary>
        /// <param name="id"><example>123456d6-1111-4a11-9c7b-eb9f14e1a32f</example></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<ActionResult> DeleteProductAsync(Guid id)
        {
            var product = await _productRepository.GetByIdAsync(id);

            if (product == null) return BadRequest(new {message = "Incorrect id"});

            await _productRepository.DeleteAsync(product);

            return Ok("Продукт удален");
        }
    }
}
