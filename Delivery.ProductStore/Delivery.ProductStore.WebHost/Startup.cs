using Delivery.ProductStore.Core.Abstractions;
using Delivery.ProductStore.DataAccess;
using Delivery.ProductStore.DataAccess.Repositories;
using Delivery.ProductStore.DataAccesss.DataInitializer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using IConfiguration = Microsoft.Extensions.Configuration.IConfiguration;
using System;
using Microsoft.EntityFrameworkCore;
using Delivery.ProductStore.Integration;
using Delivery.ProductStore.WebHost.Services;
using Blocks.Contracts.OrdersManagement;
using Blocks.Contracts.Abstractions;
using Delivery.ProductStore.Integration.AsyncDataServices;
using Delivery.ProductStore.WebHost.Hubs;

namespace Delivery.ProductStore.WebHost
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSignalR();
            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));

            services.AddScoped<IDbInitializer, EfDbInitializer>();

            services.AddDbContext<DataContext>(x => {

                x.UseNpgsql(Configuration.GetConnectionString("ProductStoreConnection"));

            }, ServiceLifetime.Singleton);

            services.AddScoped<ICachingService<Product>,ProductService>();
            services.AddMemoryCache();

            services.AddScoped<IMessageBusClient, MessageBusClient>();

            services.AddHttpClient<IGivingProductDeliveryGateway, GivingProductDeliveryGateway>(c =>
            {
                c.BaseAddress = new Uri(Configuration["IntegrationSettings:GivingToProductDeliveryApiUrl"]);
            });

            services.AddHttpClient<IGivingProductOrderGateway, GivingProductOrderGateway>(c =>
            {
                c.BaseAddress = new Uri(Configuration["IntegrationSettings:GivingToProductOrderApiUrl"]);
            });

            services.AddControllers().AddJsonOptions(options =>
            {
                options.JsonSerializerOptions.ReferenceHandler = System.Text.Json.Serialization.ReferenceHandler.Preserve;
            });
            services.AddOpenApiDocument(options =>
            {
                options.Title = "Delivery.ProductStore API";
                options.Version = "1.0";
            });
            
            services.AddCors(option =>
            {
                option.AddDefaultPolicy(
                    policy =>
                    {
                        policy.WithOrigins("http://localhost:3000",
                                "https://localhost:3000", "http://localhost:3001",
                                "https://localhost:3001", "http://localhost:3002", 
                                "https://localhost:3002")
                            .AllowAnyMethod()
                            .AllowAnyHeader();
                    });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDbInitializer dbInitializer)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseOpenApi();
            app.UseCors();

            app.UseSwaggerUi3(x =>
            {
                x.DocExpansion = "list";
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<GreetHub>("/hubs/greet");
            });

            dbInitializer.InitializeDb();
        }
    }
}
