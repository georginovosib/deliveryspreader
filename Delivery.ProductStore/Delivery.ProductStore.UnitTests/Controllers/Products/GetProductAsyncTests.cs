﻿using Blocks.Contracts.OrdersManagement;
using Delivery.ProductStore.Controllers;
using Delivery.ProductStore.Core.Abstractions;
using Delivery.ProductStore.Integration.AsyncDataServices;
using Delivery.ProductStore.WebHost.Services;
using FluentAssertions;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace Delivery.ProductStore.UnitTests.Controllers.Products
{
    public class GetProductAsyncTests
    {
        private ProductsController _productsController;
        private Mock<ILogger<ProductsController>> _logger = new Mock<ILogger<ProductsController>>();
        private Mock<IRepository<Product>> _productRepositoryMock = new Mock<IRepository<Product>>();
        private Mock<ProductService> _service = new Mock<ProductService>();
        private Mock<IGivingProductDeliveryGateway> _givingProductDeliveryGateway = new Mock<IGivingProductDeliveryGateway>();
        private Mock<IGivingProductOrderGateway> _givingProductOrderGateway = new Mock<IGivingProductOrderGateway>();
        private Mock<IMessageBusClient> _messageBusClient = new Mock<IMessageBusClient>();

        public GetProductAsyncTests()
        {
            _productsController = new ProductsController(_logger.Object, _productRepositoryMock.Object,
                 _givingProductDeliveryGateway.Object, _givingProductOrderGateway.Object, _service.Object, _messageBusClient.Object);
        }

        [Fact]
        public async void GetProductAsync_Count_3()
        {
            // Arrange
            List<Product> products = CreateListProducts();

            _productRepositoryMock.Setup(repo => repo.GetAllAsync()).ReturnsAsync(products);

            // Act
            var result = await _productsController.GetProductAsync();

            // Assert
            result.Count().Should().Be(3);
        }

        [Fact]
        public async void GetProductAsync_Name()
        {
            // Arrange
            List<Product> products = CreateListProducts();

            _productRepositoryMock.Setup(repo => repo.GetAllAsync()).ReturnsAsync(products);

            // Act
            var result = await _productsController.GetProductAsync();

            // Assert
            result.FirstOrDefault(x => x.Price == 7000).Name.Should().Be("Нежное послание");
        }

        public List<Product> CreateListProducts()
        {
            List<Product> products = new List<Product>()
            {
                 new Product()
                 {
                     Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116311"),
                     Name = "Бал цветов",
                     Amount = 5,
                     Description = "Розы",
                     Price = 4500,
                     DeliveryPrice = 500
                 },
                 new Product()
                 {
                     Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116322"),
                     Name = "Цветочная поэзия",
                     Amount = 3,
                     Description = "Орхидеи",
                     Price = 8000,
                     DeliveryPrice = 500
                 },
                 new Product()
                 {
                     Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116333"),
                     Name = "Нежное послание",
                     Amount = 7,
                     Description = "Герберы",
                     Price = 7000,
                     DeliveryPrice = 500
                 },
            };

            return products;
        }
    }
}
