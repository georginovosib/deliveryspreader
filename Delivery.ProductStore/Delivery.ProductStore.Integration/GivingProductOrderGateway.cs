﻿using Blocks.Contracts.Dto;
using Blocks.Contracts.OrdersManagement;
using Delivery.ProductStore.Core.Abstractions;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace Delivery.ProductStore.Integration
{
    public class GivingProductOrderGateway : IGivingProductOrderGateway
    {
        private readonly HttpClient _httpClient;

        public GivingProductOrderGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task GiveProductToAll(Product product)
        {
            var dto = new GiveProductToAllDto()
            {
                ProductId = product.Id,
                Name = product.Name,
                //Amount = product.Amount,
                Description = product.Description,
                Price = product.Price,
                DeliveryPrice = product.DeliveryPrice
            };

            var response = await _httpClient.PostAsJsonAsync("api/v1/products", dto);

            response.EnsureSuccessStatusCode();
        }
    }
}
