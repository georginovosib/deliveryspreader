﻿using Blocks.Contracts.Dto;
using System.Collections.Generic;

namespace Delivery.ProductStore.Integration.AsyncDataServices
{
    public interface IMessageBusClient
    {

        void SendProductToOrder(AddProductDto dto);

        void SendProductRangeToOrder(AddProductRangeDto dtos);
    }
}
