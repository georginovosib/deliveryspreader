﻿using Blocks.Contracts.Dto;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;

namespace Delivery.ProductStore.Integration.AsyncDataServices
{
    public class MessageBusClient : IMessageBusClient
    {

        private readonly IConfiguration _configuration;

        public MessageBusClient(IConfiguration configuration)
        {
            _configuration = configuration;

        }


        public void SendProductToOrder(AddProductDto dto)
        {
            var factory = new ConnectionFactory()
            {
                UserName = "admin",
                Password = "admin",
                HostName = _configuration["RabbitMQHost"],
                Port = int.Parse(_configuration["RabbitMQPort"]),
            };

            Console.WriteLine($"--> Trying to connect on: {factory.HostName}:{factory.Port}");

            try
            {
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare(exchange: "productExchange", type: ExchangeType.Fanout);

                    Console.WriteLine("--> Connected to MessageBus");
                    channel.QueueDeclare(queue: "productSendingQueue",
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);
                    channel.QueueBind(queue: "productSendingQueue",
                                      exchange: "productExchange",
                                      routingKey: "");


                    var message = JsonSerializer.Serialize(dto);

                    var body = Encoding.UTF8.GetBytes(message);

                    channel.BasicPublish(exchange: "productExchange",
                        routingKey: "",
                        basicProperties: null,
                        body: body);
                    Console.WriteLine($"--> We have sent {message}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"--> Could not connect to the Message Bus: {ex.Message}");
                Console.WriteLine($"--> InnerException: {ex.InnerException.Message}");
                throw;
            }

        }

        public void SendProductRangeToOrder(AddProductRangeDto dtos)
        {
            var factory = new ConnectionFactory()
            {
                UserName = "admin",
                Password = "admin",
                HostName = _configuration["RabbitMQHost"],
                Port = int.Parse(_configuration["RabbitMQPort"]),
            };

            Console.WriteLine($"--> Trying to connect on: {factory.HostName}:{factory.Port}");

            try
            {
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    channel.ExchangeDeclare(exchange: "productExchange", type: ExchangeType.Fanout);

                    Console.WriteLine("--> Connected to MessageBus");
                    channel.QueueDeclare(queue: "productSendingQueue",
                                 durable: false,
                                 exclusive: false,
                                 autoDelete: false,
                                 arguments: null);
                    channel.QueueBind(queue: "productSendingQueue",
                                      exchange: "productExchange",
                                      routingKey: "");


                    var message = JsonSerializer.Serialize(dtos);

                    var body = Encoding.UTF8.GetBytes(message);

                    channel.BasicPublish(exchange: "productExchange",
                        routingKey: "",
                        basicProperties: null,
                        body: body);
                    Console.WriteLine($"--> We have sent {message}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"--> Could not connect to the Message Bus: {ex.Message}");
                Console.WriteLine($"--> InnerException: {ex.InnerException.Message}");
                throw;
            }

        }

    }
}
