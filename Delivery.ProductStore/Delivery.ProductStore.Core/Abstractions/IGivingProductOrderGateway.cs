﻿using Blocks.Contracts.OrdersManagement;
using System.Threading.Tasks;

namespace Delivery.ProductStore.Core.Abstractions
{
    public interface IGivingProductOrderGateway
    {
        Task GiveProductToAll(Product product);
    }
}
