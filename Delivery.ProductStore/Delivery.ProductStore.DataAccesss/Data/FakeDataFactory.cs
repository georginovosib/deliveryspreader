﻿using Blocks.Contracts.OrdersManagement;
using System;
using System.Collections.Generic;

namespace Delivery.ProductStore.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static IEnumerable<Product> Products => new List<Product>()
        {
            new Product()
            {
                Id = Guid.Parse("123456d6-1111-4a11-9c7b-eb9f14e1a32f"),
                Name = "Притяжение",
                Amount = 5,
                Description = "Букет",
                Price = 1800,
                DeliveryPrice = 500
            },
            new Product()
            {
                Id = Guid.Parse("123456d6-2222-4a11-9c7b-eb9f14e1a32f"),
                Name = "Мелодия цветов",
                Amount = 16,
                Description = "Букет",
                Price = 2800,
                DeliveryPrice = 500
            },
            new Product()
            {
                Id = Guid.Parse("123456d6-3333-4a11-9c7b-eb9f14e1a32f"),
                Name = "Летняя свежесть",
                Amount = 17,
                Description = "Букет",
                Price = 3800,
                DeliveryPrice = 500
            },
            new Product()
            {
                Id = Guid.Parse("123456d6-4444-4a11-9c7b-eb9f14e1a32f"),
                Name = "Цветочная фантазия",
                Amount = 18,
                Description = "Букет",
                Price = 6300,
                DeliveryPrice = 500
            },
            new Product()
            {
                Id = Guid.Parse("123456d6-5555-4a11-9c7b-eb9f14e1a32f"),
                Name = "Искорка радости",
                Amount = 3,
                Description = "Букет",
                Price = 38000,
                DeliveryPrice = 500
            }
        };

    }
}
