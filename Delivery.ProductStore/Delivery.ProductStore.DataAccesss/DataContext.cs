﻿using Microsoft.EntityFrameworkCore;
using Blocks.Contracts.OrdersManagement;

namespace Delivery.ProductStore.DataAccess
{
    public class DataContext : DbContext
    {
        public DbSet<Product> Products { get; set; }

        public DataContext()
        {

        }

        public DataContext(DbContextOptions<DataContext> options)
   : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Ignore<Order>();
        }
    }
}
