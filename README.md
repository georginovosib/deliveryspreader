# DeliverySpreader

System for spread delivery between company couriers.

## Our Team

- Константин Варламов
- Юрий Шишлов
- Павел Антонов
- Гогулин Сергей
- Шевяков Антон
- Чирков Дмитрий
- 

## Links
- [GO TO BOARD](https://gitlab.com/kvarlamov/deliveryspreader/-/boards/3963320)
- [Architecture](https://docs.google.com/document/d/190Jw4sheBWZ6XoHBfOL6IgXBebB6eSsBNwPqt-kWa6A/edit?usp=sharing)

## Installation
orderUI start:
- install nodeJS (if not)
- open folder orderUI, start cmd/ powershell/gitbash/ terminal in any IDE
- print command: npm start 

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Roadmap

## License
[MIT License](https://gitlab.com/kvarlamov/deliveryspreader/-/blob/main/LICENSE)

## Project status
Development

***
# USEFUL INFORMATION

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/kvarlamov/deliveryspreader.git
git branch -M main
git push -uf origin main
```
