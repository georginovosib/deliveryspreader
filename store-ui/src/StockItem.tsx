import { HttpTransportType, HubConnection, HubConnectionBuilder, LogLevel } from "@microsoft/signalr";
import { Button } from "antd";
import React, { useEffect, useState } from "react";

const StockItem = () => {
    const [connection, setConnection] = useState<null | HubConnection>(null);
    const [name, setname] = useState("");
    const [price, setprice] = useState(0);
    const [productid, setproductid] = useState(""); 
    const [deliveryprice, setdeliveryprice] = useState(0); 

    useEffect(() => {

        console.log("start");

        const connect = new HubConnectionBuilder()
            .configureLogging(LogLevel.Debug)
            .withUrl("http://localhost:8083/hubs/greet", {
                skipNegotiation: true,
                transport: HttpTransportType.WebSockets
            })
            .withAutomaticReconnect()
            .build();

        setConnection(connect);
    }, []);

    useEffect(() => {
        if (connection) {
            connection
                .start()
                .then(() => {
                    console.log("connect to stock info");
                    connection.on("Receive", handleReceiveMessage);
                })
                .catch((error) => console.log(error));
        }
    }, [connection]);

    const handleReceiveMessage = (productid: string, name: string, price: number, deliveryprice: number) => {
        console.log("name: " + name);

        setproductid(productid);
        setname(name);
        setprice(price);
        setdeliveryprice(deliveryprice);
    }

    const sendMessage = async () => {
        if (connection)
            await connection.send("GetProductByIdAsync", "123456d6-1111-4a11-9c7b-eb9f14e1a32f");
    };

    return (
        <>

            <Button onClick={sendMessage} type="primary">
                Get
            </Button>

            <h1>productid: {productid}</h1>
            <h1>name: {name}</h1>
            <h1>price: {price}</h1>
            <h1>deliveryprice: {deliveryprice}</h1>
        </>
    );
};

export default StockItem;